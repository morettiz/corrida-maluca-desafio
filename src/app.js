const express = require('express');

const WackyRace = require('./app/WackyRace');

class App {
    constructor() {
        this.server = express();

        this.init();
    }

    init() {
        WackyRace.start();

        var finalists = WackyRace.finish();

        finalists = WackyRace.adapt(finalists);

        WackyRace.printFinalists(finalists);
    }

}

module.exports = new App().server;