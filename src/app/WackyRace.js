const { 
    startOfDay, setSeconds, setMinutes, setMilliseconds, 
    addSeconds, addMinutes, addMilliseconds, isBefore, 
    format, parseISO 
} = require('date-fns');

const Log = require('../logs/LogFormatted');

const time = 0;
const carInfo = 1;
const lap = 2;
const lapTime = 3;
const averageSpeed = 4;
const finalLap = 4;

const date = new Date();

class WackyRace {
    start() {
        this.logs = Log.getLogs();
    }

    finish() {
        var raceTotalElapsedTime = [];
        for (var raceLog of this.logs) {
            const [minute, secondsWithMilliseconds] = raceLog[lapTime].split(':');
            const [seconds, millisenconds] = secondsWithMilliseconds.split('.');

            if (raceTotalElapsedTime[raceLog[carInfo]]) {
                raceTotalElapsedTime[raceLog[carInfo]].lap = raceLog[lap];
                const totalTimeUpToNow = raceTotalElapsedTime[raceLog[carInfo]].lapTime;
                raceTotalElapsedTime[raceLog[carInfo]].lapTime = 
                addMilliseconds(
                    addSeconds(
                        addMinutes(totalTimeUpToNow, minute), seconds), millisenconds);
            } else {
                raceTotalElapsedTime[raceLog[carInfo]] = {
                    lap: raceLog[lap],
                    lapTime: 
                    setMilliseconds(
                        setSeconds(
                            setMinutes(
                                startOfDay(date), minute), seconds), millisenconds),
                };
            }

            if (raceLog[lap] === finalLap) {
                return raceTotalElapsedTime;
            }
        }
    }

    adapt(raceTotalElapsedTime) {
        var finalists = Object.keys(raceTotalElapsedTime).map(function (key) {
            return {
                car: key,
                lap: raceTotalElapsedTime[key].lap,
                totalTime: format(raceTotalElapsedTime[key].lapTime, 'mm:ss.SSS'),
            };
        });

        return this.sort(finalists);
    }

    sort(finalists) {
        return finalists.sort(function (a, b) {
            if (a.lap == b.lap) {
                return isBefore(parseISO(a.totalTime), parseISO(b.totalTime)) ? 1 : -1;
            }

            return (a.lap > b.lap) ? -1 : 1;
        });
    }

    printFinalists(finalists) {
        console.log('\n************** Fim da Corrida **************' + '\n\n' )
        var i = 1;
        finalists = finalists.map(function (finalist) {
            return {
                'Posição Chegada': i++,
                'Carro': finalist.car,
                'Qntde Voltas Completadas': finalist.lap,
                'Tempo Total de Prova': finalist.totalTime,
            };
        });

        console.log(finalists);
    }
}

module.exports = new WackyRace();